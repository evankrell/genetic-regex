#ifndef REGEGPIPEUTILS_H
#define REGEXPIPEUTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "regexScanner.h"
#include "regexlist.h"
#include "regexgen_algs.h"

/* This library contains the constants and major pipeline functions for 
 * GP regular expressions. 
 * It does *not* specify the actual algorithms used for generation and mutation
 */

static int Z_LEN = 61;
static int OP_LEN = 4;

/* The alphabet that the regular expression is over */
static char Z[62] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'u', 'j', 'k', 'l', 'm', 'n', 'o',
                    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 
                    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
                    'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', 
                    '9','0' };

static char OPS[4] = { '*', '|', '(', ')' };


pool *generation (int n, int d, int l, void (**methods)(char *, int, int, char **));


   /*******************************************
    **** SECTION A: Evolutionary Techniques****
    *******************************************/

/***********************
 ***** Generation ******
 ***********************/

pool *generation (int n, int d, int l, void (**methods)(char *, int, int, char **))
{
   /* combine Z and OPS to list of symbols */
   char *syms = (char *) malloc (sizeof (char) * (Z_LEN + OP_LEN + 1));
   strncpy (syms, Z, Z_LEN);
   strncpy (syms + Z_LEN, OPS, OP_LEN);
   int numSyms = Z_LEN + OP_LEN;
   syms[numSyms] = '\0';

   /* individual data */
   char *regexStr = NULL;
   
   /* initialize pool */
   pool *newPool = initPool ();

   /* generate n individuals */
   int depth, length;
   int i;
   for (i = 0; i < n; i++)
   {
      /* random depth, random size */
      depth = (int) random_at_most ((long) d - 1.0) + 1;
      length = (int) random_at_most ((long) l - 1.0) + 1;

      /* select algorithm */
      if (methods == NULL)
      {
         /* choose default method */
         generate_2 (syms, numSyms - 1, length, &regexStr);
      }
      else
      {
         /* choose from array of input methods */
/* TEMP: Does not use index. Should randomly select one of the methods */
         (*methods[0]) (syms, numSyms - 1, length, &regexStr);
         //addToPool (newPool, "--", 0);
      }

      /* add to pool */
      addToPool (newPool, regexStr, 0);
   }

   return newPool;
}


/***********************
 **** Reproduction *****
 ***********************/

pool *reproduction (pool *P, int n, int d, int l, int m, int gen, void (**methods))
{
   /* individual data */
   char *regexStr;

   /* initialize pool */
   pool *newPool = initPool ();

   /* parents */
   regind *male;
   regind *female;
   int mIdx = 0;
   int fIdx = 0;

   int i;
   int depth, length;
   for (i = 0; i < m; i++)
   {
      /* random depth, random size */
      depth = (int) random_at_most ((long) d - 1.0) + 1;
      length = (int) random_at_most ((long) l - 1.0) + 1;

      /* select 2 random pool members */
         /* atm, can be the same. Consider changing */
         /* or simply don't take the biology analogy too far */
      mIdx = random_at_most (P->count);
      fIdx = random_at_most (P->count);
      male = ithPoolMember (P, mIdx);
      female = ithPoolMember (P, fIdx);

      /* select reproduction method */
      if (methods == NULL)
      {
         /* choose default method */
         reproduce (male->str, female->str, length, &regexStr);

      }
      else
      {
         /* choose from array of input methods */
/* TEMP: Does not use index */
         //(*methods[0]) (alaalff)
      }

      /* add to pool */
      addToPool (newPool, regexStr, gen);
   }
   return newPool;
}


/***********************
 ****** Mutation *******
 ***********************/

pool *mutation (pool *P, int n, int d, int l, int m, int gen, char *syms, int numSyms, void (**methods))
{
   /* individual data */
   char *regexStr;

   /* initialize pool */
   pool *newPool = initPool ();

   /* mutation base */
   regind *mutBase;
   int mutBaseIdx = 1;


   int i;
   int depth, length;
   for (i = 0; i < m; i++)
   {
      /* random depth, random size */
      depth = (int) random_at_most ((long) d - 1.0) + 1;
      length = (int) random_at_most ((long) l - 1.0) + 1;

      /* select a random regind to serve
       * as the basis of mutation */
      mutBaseIdx = random_at_most (P->count);
      mutBase = ithPoolMember (P, mutBaseIdx);

       /* select reproduction method */
      if (methods == NULL)
      {
         /* choose default method */
         mutate (mutBase->str, 0, syms, numSyms, &regexStr);
      }
      else
      {
         /* choose from array of input methods */
/* TEMP: Does not use index */
         //(*methods[0]) (alaalff)
      }
      /* add to pool */
      addToPool (newPool, regexStr, gen);
   }
   return newPool;
}


   /*******************************************
    ******* SECTION C: Pool Annotation ********
    *******************************************/

int annParsePool (pool *P)
{

      /* MODIFIES:
       *  root    adds a syntax tree
       *  pass    determines via syntax tree if valid
       */

   /* While it may change in the future,
    * the current version uses the language 
    * and parsing defined by an include
    * instead of accepting function ptr */

   regind *cur = P->head;

   int i, res;
   for (i = 0; i < P->count; i++)
   {
      cur->pass = parse (cur->str, cur->root);
      cur = cur->link;
   }

}



   /*******************************************
    ****** SECTION B: Pipeline Utilities ******
    *******************************************/

pool *filter (pool *P, int (*method) (regind *, int *))
{
   /* flag to continue filtering */
   int *cFlag;  /* start out as true */
   cFlag = (int *) malloc (sizeof (int));
   *cFlag = 1;

   pool *newPool;

   if (method == NULL)
   {
      /* no filter method specified
       * and no default exists..
       * Just return a copy */
      newPool = copyPool (P);
   }
   else        /* USE METHOD */
   { 
      newPool = initPool ();
      regind *cur = P->head;
      int n = P->count;
      int i;
      int res = 0;

      for (i = 0; i < n; i++)
      {
         if (*cFlag <= 0)
         {
            return newPool;
         }
         res = (method) (cur, cFlag);
         if (res > 0)
         {
            addToPool (newPool, cur->str, 0);
            newPool->tail->score = cur->score;
            newPool->tail->generation = cur->generation;
         }
      }
   }
   return newPool;
}



/****************
 **** Driver ****
 ****************/

int regexpipeutils_driver ()
{
   /* TEST: Generation */
   int numInds = 100;
   int maxDepth = 40;
   int maxLength = 50;

   int gen = 0;

   /* call default method */
   pool *testPool = generation (numInds, maxDepth, maxLength, NULL);
   printPool (testPool);

   /* call custom method */
   void (*functions[1]) (char *syms, int symLen, int max, char **outStr);
   functions[0] = generate_2;
   testPool = generation (numInds, maxDepth, maxLength, functions);
   printPool (testPool);

   /* TEST: Reproduction */
   int numChildren = 30;
   gen++;
   pool *repPool = reproduction (testPool, numInds, maxDepth, maxLength, numChildren, gen, NULL);
   printPool (repPool);

   /* TEST: Mutation */
   char *syms = (char *) malloc (sizeof (char) * (Z_LEN + OP_LEN + 1));
   strncpy (syms, Z, Z_LEN);
   strncpy (syms + Z_LEN, OPS, OP_LEN);
   syms[Z_LEN + OP_LEN] = '\0';
   int numSyms = Z_LEN + OP_LEN;

   int numMutants = 60;
   gen++;
   pool *mutPool = mutation (testPool, numInds, maxDepth, maxLength, numMutants, gen, syms, numSyms - 1, NULL);
   printPool (mutPool);



/* REPEAT */

   testPool = generation (numInds, maxDepth, maxLength, NULL);
   printPool (testPool);

   /* call custom method */
   functions[0] = generate_2;
   testPool = generation (numInds, maxDepth, maxLength, functions);
   printPool (testPool);

   /* TEST: Reproduction */
   numChildren = 30;
   gen++;
   repPool = reproduction (testPool, numInds, maxDepth, maxLength, numChildren, gen, NULL);
   printPool (repPool);

   /* TEST: Mutation */
   syms = (char *) malloc (sizeof (char) * (Z_LEN + OP_LEN + 1));
   strncpy (syms, Z, Z_LEN);
   strncpy (syms + Z_LEN, OPS, OP_LEN);
   syms[Z_LEN + OP_LEN] = '\0';
   numSyms = Z_LEN + OP_LEN;

   numMutants = 60;
   gen++;
   mutPool = mutation (testPool, numInds, maxDepth, maxLength, numMutants, gen, syms, numSyms - 1, NULL);
   printPool (mutPool);

/* END REPEAT */


/* TEST: (no) Filter */
   printf ("FILTERING...\n");
   pool *filtered = filter (mutPool, NULL);
   printf ("FILTERED:\n\n");
   printPool (filtered);



   /* Test: annotation, parse */
   printf ("PARSING...\n");
   annParsePool (testPool);
   //printPool (testPool);


}

#endif
