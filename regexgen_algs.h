#ifndef REGEXGEN_ALGS
#define REGEXGEN_ALGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//static int Z_LEN = 61;
//static int OP_LEN = 4;
//
///* The alphabet that the regular expression is over */
//static char Z[62] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'u', 'j', 'k', 'l', 'm', 'n', 'o',
//                    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 
//                    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
//                    'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', 
//                    '9','0' };
//
//static char OPS[4] = { '*', '|', '(', ')' };
//

/* utility functions */
char *sample (char *sampleSpace, int sampleLen, int resultLen);
long random_at_most ();

/* spontatneous regex generation */
   /* but where did the regex come from :^) ? */
void generate_1 (char *Z, int Z_LEN, int max, char **outStr);
void generate_2 (char *Z, int Z_LEN, int maz, char **outStr);


   /* CREDIT FOR THE FOLLOWING SEGMENT 
    * Ryan Reich
    * https://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range#6852396
    */
// Assumes 0 <= max <= RAND_MAX
// Returns in the half-open interval [0, max]
long random_at_most(long max) {
  unsigned long
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
    num_bins = (unsigned long) max + 1,
    num_rand = (unsigned long) RAND_MAX + 1,
    bin_size = num_rand / num_bins,
    defect   = num_rand % num_bins;

  long x;
  do {
   x = random();
  }
  // This is carefully written not to overflow
  while (num_rand - defect <= (unsigned long)x);

  // Truncated division is intentional
  return x/bin_size;
}
   /* CREDIT FOR THE PRECEDING SEGMENT 
    * Ryan Reich
    * https://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range#6852396
    */


char *sample (char *sampleSpace, int sampleLen, int resultLen)
{
   /* performs sampling WITH replacement */
   /* does not seed rand */
//char *result = malloc (sizeof (char) * (sampleLen + 1));
   char *result = calloc (sampleLen + 1, sizeof (char));

   long max = (long ) sampleLen - 1;

   int i;
   for (i = 0; i < resultLen; i++)
   {
      int j = (int) random_at_most (max);
      result[i] = sampleSpace[ j ];
   }
   result[sampleLen - 1] = '\0';

   return result;
}


/* most rudamentary regex generation */
/* does not guarantee a valid regex */
void generate_1 (char *syms, int symLen, int max, char **outStr)
{
   *outStr = sample (syms, symLen, max);
   //return sample (syms, symLen, max);
   return;
}

/* builds on generate_1 () by ensuring balanced parens */
void generate_2 (char *syms, int symLen, int max, char **outStr)
{
   char *str = NULL;
   generate_1 (syms, symLen, max, &str);

   int i;
   int r = 0;
   int numTries = 0;
   int numLParens = 0;
   for (i = 0; i < max; i++)
   {
      if (str[i] == '(')
      {
         while (str[r] != ')' && numTries < 5)
         {
            r = random_at_most (max - 1 - i) + i + 1;
            //printf ("%d\n", r);
            str[r] = ')';
         }
         while (str[r] != ')' && str[i] == '(') /* ran out of tries */
         {
            /* so just replace the '(' with something else */
            str[i] == syms [(int) random_at_most (max - 1)];
         }
         
         r = 0;
            
      }
   }
   *outStr = strdup (str);
   //return str;   
   return;
}



void reproduce (char *m, char *f, int max, char **outStr)
{
   char **parents = (char **) malloc (sizeof (char *) * 2);
   parents[0] = strdup (m);
   parents[1] = strdup (f);

//printf ("MALE = %s\nFEMALE = %s\n", parents[0], parents[1]);

//*outStr = (char *) malloc (sizeof (char) * max);
   *outStr = calloc (max, sizeof (char));
   
   int remains = max;
   int whichParent = 0;
   int subLen = 0;
   int startPos = 0;
   int pos = 0;

   /* while the outStr has not reached max length
    * continue to add substrings from m and f */
   while (remains > 0)
   {
      /* choose which parent */
      whichParent = random_at_most (1);
   
      /* choose length of substring */
      subLen = random_at_most (remains - 1) + 1;

      if (subLen > strlen (parents[whichParent]))
      {
         subLen = strlen (parents[whichParent]);

      }
   //printf ("sublen = %d\n", subLen);
      startPos = random_at_most (strlen (parents[whichParent]) - subLen);        
   //printf ("startpos = %d\n", startPos);

      strncpy (*outStr + pos, parents[whichParent] + startPos, subLen - 1);
      remains = remains - subLen;
      pos = pos + subLen;
   }
}

void mutate (char *base, int max, char *syms, int numSyms, char **outStr)
{
   /* choose whether add, delete, or replace a gene (character) */
   int randSelect = random_at_most ((long) 2);   /* 0, 1, 2 */
   int randIdx = 0;
   int randSymIdx = 0;

   if (strlen (base) == 1 && randSelect != 2)
   {
      randSelect = 2;     /* PROBABILITY BIAS. */
   }

   int i;
   int j;
   int checkPoint = 0;

   switch (randSelect){
      case 0 : /* ADD */
//*outStr = (char *) malloc (sizeof (char) * (strlen (base) + 1));
         outStr[0] = calloc (strlen (base) + 1, sizeof (char));
for (j = 0; j < (strlen (base) - 1); j++)
{
   outStr[0][j] = ' ';
}
         outStr[0][strlen (base)] = '\0';
         randIdx = random_at_most ((long) strlen (base) - 1);
         randSymIdx = random_at_most ((long) numSyms - 1);
         for (i = 0; i < strlen (*outStr); i++)
         {
            if (i == randIdx)
            {
               randIdx = random_at_most ((long)numSyms - 1);
               outStr[0][i] = syms[randSymIdx];
            }
            else
            {
               if (checkPoint == 0)
               {
                  outStr[0][i] = base[i];
                  checkPoint = 1;
               }
               else
               {
                  outStr[0][i] = base[i - 1];
               }
            }
         }
         break;
      case 1 : /* DELETE */
//*outStr = (char *) malloc (sizeof (char) * (strlen (base) - 1));
         outStr[0] = calloc (strlen (base) - 1, sizeof (char));
for (j = 0; j < (strlen (base) - 2); j++)
{
outStr[0][j] = ' ';
}
         outStr[0][strlen (base) - 2] = '\0';
         randIdx = random_at_most ((long) strlen (base) - 2);
         for (i = 0; i < strlen (*outStr); i++)
         {
            if (i == randIdx)
            {
               checkPoint = 1;
            }
            else
            {
               if (checkPoint > 0)
               {
                  outStr[0][i] = base[i];
               }
               else
               {
                  outStr[0][i] = base[i + 1];
               }
            }
         }
         break;
      case 2 : /* REPLACE */
         *outStr = strdup (base);
         randIdx = random_at_most ((long) (strlen (*outStr) - 1));
         randSymIdx = random_at_most ((long) numSyms);
         printf ("SYM = %c\n\n", syms[randSymIdx]);
         outStr[0][randIdx] = syms[randSymIdx];
         break;
      default : /* ERROR */
         printf ("Ouch...\n");
         break;
   }
}




//int regexgen_algs_driver ()
//{
//
//   srand (time (NULL));
//
//   printf ("RANDOM STRING: %s\n", sample (Z, Z_LEN, 10));
//
//   char *syms = (char *) malloc (sizeof (char) * (Z_LEN + OP_LEN + 1));
//   strncpy (syms, Z, Z_LEN);
//   strncpy (syms + Z_LEN, OPS, OP_LEN);
//   syms[Z_LEN + OP_LEN] = '\0';
//
//   printf ("%s\n", syms);
//
//   char *test;
//   generate_1 (syms, Z_LEN + OP_LEN - 1, 10, &test);
//   printf ("RANDOM REGEX: %s\n", test);
//
//   /* !!! BAD VERSION, as it does not scale to more symbols
//    * In order to not have ')', just take one from the length, since
//    * the ')' is the last symbol in syms 
//    */
//
//   int i;
//   for (i = 0; i < 1000; i++)
//   {
//      char * reg_gen_2;
//      generate_2 (syms, Z_LEN + OP_LEN - 1, 10, &reg_gen_2);
//      int hasParen = 0;
//      int k;
//      for (k = 0; k < 10; k++)
//      {
//         if (reg_gen_2[k] == '(')
//         {
//            hasParen = 1;
//            break;
//         }
//      }
//      if (hasParen == 1)
//      {
//         printf ("BALANC REGEX: %s\n", reg_gen_2);
//      }
//
//   }
//
//   return 0;
//}
//


#endif
