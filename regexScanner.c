#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "regextree.h"

static int VERBOSE = 1;

static char TOKEN;
static int TIDX;
char *STR;
char *OSTR;

#define ERRINVTOK -1;


/* Instead of using numeric tokens, using single characters
 * since regex works using single characters having individual meaning.
 * To make token parsing simpler, all alphabetical characters are changed to "c"
 * and all numeric characters are changed to "n" */

int tokenize (char *str, int len);
int match (char expected);
int getToken ();
void printTokens ();


int regex (treeNode *p);
int term (treeNode *p);
int factor (treeNode *p);
int base (treeNode *p);

int tokenize (char *str, int len)
{

   /* in order to not deal with strlen 
    * after this part, it would be good to
    * ensure that output is a good string */

   /* save as OSTR */
   OSTR = (char *) malloc (sizeof (char) * len);
   strcpy (OSTR, str);
   OSTR[len] = '\0';

   int i;
   for (i = 0; i < len; i++)
   {
      if (str[i] != '\0')
      {
         if (str[i] >= 48 && str[i] <= 57)
         {
            str[i] = 'n';
         }
         else if ( (str[i] >= 67 && str[i] <= 90) ||
                   (str[i] >= 97 && str[i] <= 122) )
         {
            str[i] = 'c';
         }
      }
   }
}

int match (char expected)
{
   /* ensure that next token is as expected */
   if (TOKEN == expected)
   {
      /* by returning 0, indicate that
       * it is okay to get next token */
      return 0;
   }
   else
   {
      /* return invalid token */
      return ERRINVTOK;
   }
}

int getToken ()
{
   /* negative return: error
    * >= 0: success   */
   int success = -1;
   if (TIDX < strlen (STR))
   {
      if (STR[TIDX] != '\0')
      {
         TOKEN = STR[TIDX];
         TIDX++;
         success = 1;
      }
   }
   else
   {
      TOKEN = '!';
   }

   if (VERBOSE)
      printf ("%c ", TOKEN);
   return success;
}

void printTokens ()
{
   /* without parsing, print each token */
   printf ("%s\n", STR);
}


int parse (char *str, treeNode *root)
{
   /* set input string as the global parsing target */
   STR = (char *) malloc (sizeof (char) * strlen (str));
   strcpy (STR, str);
   STR[strlen (str)] = '\0';
   TIDX = 0;

   if (VERBOSE)
      printTokens ();

   /* assumes an already initialized tree */
   int ret = regex (root);

   return ret;
  
}


int regex (treeNode *p)
{
   /* 
    * regex    ->    term '|' regex | term
    */

   if (VERBOSE)
      printf ("REGEX, ");

   int ret = 0;
   
   /* add STMT node */
   treeNode *a = newSTMTNode (REGEX);
   a->parent = p;
   addChild (p, a);

   ret = ret + term (a);
   if (ret < 0)
      return ret;


   if (TOKEN == '|')
   {
      ret = ret + match ('|');

      /* add VALUE node */
      treeNode *b = newVALUENode (OPERAND, TOKEN, TOKEN, TIDX - 1, 0);
      b->parent = a;
      addChild (a, b);


      ret = ret + regex (a);
   }
   else if (TOKEN == '!')
   {
      /* ACCEPT STATE */
      return ret;
      
   }

   return ret;
}

int term (treeNode *p)
{
   /*
    * term  ->    {factor}
    */

   if (VERBOSE)
      printf ("TERM, ");

   int ret = 0;

   /* add STMT node */
   treeNode *a = newSTMTNode (TERM);
   a->parent = p;
   addChild (p, a);

   getToken ();

   ret = ret + factor (a);
   if (ret < 0)
      return ret;

   getToken ();

   while (TOKEN == 'n' || TOKEN == 'c')
   {
      
      ret = ret + factor (a);
      if (ret < 0)
         return ret;

      getToken ();
   }

   return ret;
}   
      


int factor (treeNode *p)
{
   /*
    * factor   ->    base {'*'}
    */

   if (VERBOSE)
      printf ("FACTOR, ");

   int ret = 0;

   /*add STMT node */
   treeNode *a = newSTMTNode (FACTOR);
   a->parent = p;
   addChild (p, a);

   ret = ret + base (a);
   if (ret < 0)
      return ret;

   while (TIDX < strlen (STR) && STR[TIDX] == '*')
   {
      getToken ();
      ret = ret + match ('*');

      /* add VALUE node */
      treeNode *b = newVALUENode (OPERAND, TOKEN, TOKEN, TIDX - 1, 0);
      b->parent = a;
      addChild (a, b);

      if (ret < 0)
         return ret;
   }

   return ret;
}

int base (treeNode *p)
{
   /******************************************
    * base  ->    char | '\'chat | '('regex')'
    ******************************************/

   if (VERBOSE)
      printf ("BASE, ");

   int ret = 0;

   /* add STMT node */
   treeNode *a = newSTMTNode (BASE);
   a->parent = p;
   addChild (p, a);

   treeNode *b;

   switch (TOKEN)
   {
      case 'n' : 
         ret = ret + match ('n');
         /* add VALUE node */
         b = newVALUENode (OPERAND, TOKEN, OSTR[TIDX-1], TIDX - 1, 0);
         b->parent = a;
         addChild (a, b);
         break;
      case 'c' :
         ret = ret + match ('c');
         /* add VALUE node */
         b = newVALUENode (OPERAND, TOKEN, OSTR[TIDX-1], TIDX - 1, 0);
         b->parent = a;
         addChild (a, b);
         break;
      case '\\' :
         ret = ret + match ('\\');
         getToken ();
            printf ("%c ", TOKEN);
         if (TOKEN == 'n')
         {
            ret = ret + match ('n');
         }
         else if (TOKEN == 'c')
         {
            /* add VALUE node */
            b = newVALUENode (OPERAND, TOKEN, OSTR[TIDX-1], TIDX - 1, 1);
            b->parent = a;
            addChild (a, b);
            ret = ret + match ('c');
         }
         else 
         {
            return -1;
         }
         break;
      case '(' :
         ret = ret + match ('(');
         /* add VALUE node */
         b = newVALUENode (OPERAND, TOKEN, TOKEN, TIDX - 1, 0);
         b->parent = a;
         addChild (a, b);
         ret = ret + regex (a);
         ret = ret + match (')');
         /* add VALUE node */
         b = newVALUENode (OPERAND, TOKEN, TOKEN, TIDX - 1, 0);
         b->parent = a;
         addChild (a, b);
         break;
      default :
         return -1;
         break;
   }

   return ret;
}
   



int main (int argc, char **argv)
{
   if (argc < 2)
   {
      printf ("Err");
      exit (-1);
   }

   tokenize (argv[1], strlen (argv[1]));

   treeNode *root = initRoot ();

   int success = parse (argv[1], root);

   if (success == 0)
   {
      printf ("Pass\n");
      printTree (root);
   }
   else
   {
      printf ("Fail\n");
   }

   return 0;
}
