#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static int Z_LEN = 61;
static int OP_LEN = 4;

/* The alphabet that the regular expression is over */
static char Z[62] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'u', 'j', 'k', 'l', 'm', 'n', 'o',
                    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 
                    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 
                    'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', 
                    '9','0' };

static char OPS[4] = { '*', '|', '(', ')' };


/* utility functions */
char *sample (char *sampleSpace, int sampleLen, int resultLen);
long random_at_most ();

/* spontatneous regex generation */
   /* but where did the regex come from :^) ? */
char *generate_1 (char *Z, int Z_LEN, int max);



   /* CREDIT FOR THE FOLLOWING SEGMENT 
    * Ryan Reich
    * https://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range#6852396
    */
// Assumes 0 <= max <= RAND_MAX
// Returns in the half-open interval [0, max]
long random_at_most(long max) {
  unsigned long
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
    num_bins = (unsigned long) max + 1,
    num_rand = (unsigned long) RAND_MAX + 1,
    bin_size = num_rand / num_bins,
    defect   = num_rand % num_bins;

  long x;
  do {
   x = random();
  }
  // This is carefully written not to overflow
  while (num_rand - defect <= (unsigned long)x);

  // Truncated division is intentional
  return x/bin_size;
}
   /* CREDIT FOR THE PRECEDING SEGMENT 
    * Ryan Reich
    * https://stackoverflow.com/questions/2509679/how-to-generate-a-random-number-from-within-a-range#6852396
    */


char *sample (char *sampleSpace, int sampleLen, int resultLen)
{
   /* performs sampling WITH replacement */
   /* does not seed rand */
   char *result = (char *) malloc (sizeof (char) * sampleLen);

   long max = (long ) sampleLen - 1;

   int i;
   for (i = 0; i < resultLen; i++)
   {
      int j = (int) random_at_most (max);
      result[i] = sampleSpace[ j ];
   }

   return result;
}


/* most rudamentary regex generation */
/* does not guarantee a valid regex */
char *generate_1 (char *syms, int symLen, int max)
{
   return sample (syms, symLen, max);
}

/* builds on generate_1 () by ensuring balanced parens */
char *generate_2 (char *syms, int symLen, int max)
{
   char *str = generate_1 (syms, symLen, max);

   int i;
   int r = 0;
   int numTries = 0;
   int numLParens = 0;
   for (i = 0; i < max; i++)
   {
      if (str[i] == '(')
      {
         while (str[r] != ')' && numTries < 5)
         {
            r = random_at_most (max - 1 - i) + i;
            //printf ("%d\n", r);
            str[r] = ')';
         }
         while (str[r] != ')' && str[i] == '(') /* ran out of tries */
         {
            /* so just replace the '(' with something else */
            str[i] == syms [(int) random_at_most (max - 1)];
         }
         
         r = 0;
            
      }
   }
   return str;   
}




int main ()
{

   srand (time (NULL));

   printf ("RANDOM STRING: %s\n", sample (Z, Z_LEN, 10));

   char *syms = (char *) malloc (sizeof (char) * (Z_LEN + OP_LEN + 1));
   strncpy (syms, Z, Z_LEN);
   strncpy (syms + Z_LEN, OPS, OP_LEN);
   syms[Z_LEN + OP_LEN] = '\0';

   printf ("%s\n", syms);

   printf ("RANDOM REGEX: %s\n", generate_1 (syms, Z_LEN + OP_LEN, 10));

   /* !!! BAD VERSION, as it does not scale to more symbols
    * In order to not have ')', just take one from the length, since
    * the ')' is the last symbol in syms 
    */

   int i;
   for (i = 0; i < 1000; i++)
   {
      char * reg_gen_2 = generate_2 (syms, Z_LEN + OP_LEN - 1, 10);
      int hasParen = 0;
      int k;
      for (k = 0; k < 10; k++)
      {
         if (reg_gen_2[k] == '(')
         {
            hasParen = 1;
            break;
         }
      }
      if (hasParen == 1)
      {
         printf ("BALANC REGEX: %s\n", reg_gen_2);
      }

   }

   return 0;
}




