/* Utility: regular expression syntax tree
 * Evan Krell
 */

#ifndef REGEXTREE_H
#define REGEXTREE_H

#include <stdio.h>
#include <stdlib.h>

typedef enum { STMT, VALUE } nodeType;
typedef enum { REGEX, TERM, FACTOR, BASE } stmtType;
typedef enum { LITERAL, OPERAND } valType;

typedef struct treeNode
{
   struct treeNode *children;
   struct treeNode *sibling;
   struct treeNode *parent;
   int numChildren;

   nodeType nodeType;
   stmtType sType;
   valType vType;
   
   char token;
   char val;
   int idx;

   int isEscaped;

} treeNode;

treeNode *newOpNode (stmtType s);
//treeNode *newConstNode (constType cons);
//treeNode *newOrgNode (orgType org);
void printTree (treeNode * startNode);

treeNode *initRoot ()
{
   treeNode *t = (treeNode *) malloc (sizeof (treeNode));
   t->token = '.';
   t->val = '.';
   t->idx = -1;

   t->children = t->sibling = t->parent = NULL;
   t->numChildren = 0;

   return t;
}

void addChild (treeNode *parent, treeNode *child)
{
   if (parent->numChildren == 0)
   {
      parent->children = child;
   }
   else
   {
      treeNode *c = parent->children;

      if (parent->numChildren == 1)
      {
         c->sibling = child;
      }
      else
      {
         int i;
         for (i = 0; i < parent->numChildren -1; i++)
         {
            c = c->sibling;
         }
         c->sibling = child;
      }
   }
   parent->numChildren++;
}
         

treeNode *newSTMTNode (stmtType s)
{
   /* always interior */

   treeNode * t = (treeNode *) malloc (sizeof (treeNode));
   t->nodeType = STMT;
   t->sType = s;
   //t->token = tok;
   //t->val = tok;
   //t->idx = idx;

   /* Set relationships within parser? */
   t->children = t->sibling = t->parent = NULL;
   t->numChildren = 0;

   return t;
}

treeNode *newVALUENode (valType v, char tok, char c, char idx, int isEscaped)
{
   /* always a leaf */

   treeNode * t = (treeNode *) malloc (sizeof (treeNode));
   t->nodeType = VALUE;
   t->vType = v;

   t->token = tok;
   t->val = c;
   t->idx = idx;
   
   t->isEscaped = isEscaped;

   /* init relationships */
   t->children = t->sibling = t->parent = NULL;
   t->numChildren = -1;

   return t;
}



void printTree (treeNode *t)
{
   if (t->numChildren > 0)
   {
      treeNode *c = t->children;

      int i;
      for (i = 0; i < t->numChildren; i++)
      {

         if(c->nodeType == STMT)
         {
            printTree (c);
         }
         else
         {
            printf ("%c", c->val);
         }

         c = c->sibling;
      }
   }
   else
   {
      printf (".");
   }
}
     
      




#endif
