/* linked list to hold a pool's data */

#ifndef REGEXLIST_H
#define REGEXLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "regextree.h"

/* Attributes of an individual regex */
typedef struct regind 
{
   char *str;        /* the actual regular expression */
   treeNode *root;   /* the associated parse tree */
   int pass;         /* whether the regind passes a syntax parse */
                     /* -1 : unchecked
                      *  0 : FAIL
                      *  1 : PASS
                      */
   long score;       /* fitness of regind */
   int generation;   /* from which generation did regind spawn */
   char *byMethod;   /* name of function that generated this regind */
   struct regind *link;
} regind;

typedef struct pool
{
   regind *head;
   regind *tail;
   int count;
} pool;

void printPool (pool *P);
regind *addToPool (pool *P, char *strIn, int genIn);

void printPool (pool *P)
{
   regind *current = P->head;
   int i;
   int stop = P->count;
   for (i = 0; i < stop; i++)
   {
      printf ("%s\n", current->str);
      current = current->link;
   }
   
}

pool *initPool ()
{
   pool *p = (pool *) malloc (sizeof (pool));
   p->count = 0;
   p->head = NULL;
   p->tail = NULL;
   return p;
}

pool *copyPool (pool *P)
{
/* WARNING:
 * DOES NOT YET COPY "byMethod" and "root"
 * since they require deep copy
 */

   pool *copyPool = initPool ();
   regind *newRegPrev = NULL;
   regind *newReg = NULL;

   regind *curOldReg = P->head;
   int n = P->count;

   copyPool->head = newReg;

   int i;
   for (i = 0; i < n; i++)
   {
      newReg = addToPool (copyPool, curOldReg->str, 0);

      newReg->score = curOldReg->score;
      newReg->generation = curOldReg->generation;

      if (newRegPrev != NULL)
      {
         newRegPrev->link = newReg;
      }

      curOldReg = curOldReg->link;
      newRegPrev = newReg;
   }

   copyPool->count = P->count;
   newRegPrev->link = newReg;
   copyPool->tail = newReg;

   return copyPool;
}
      


regind *addToPool (pool *P, char *strIn, int genIn)
{
   regind *r = (regind *) malloc (sizeof (regind));
   r->str = strdup (strIn);
   r->generation = genIn;
   r->root = initRoot ();
   r->pass = -1;  /* has not been checked */
   r->score = 0;
   r->link = NULL;

   /* add to P */
   if (P->head == NULL)    /* if pool is empty */
   {
      P->head = r;
      P->tail = P->head;
   }
   else
   {
      P->tail->link = r;
      P->tail = r;
   }

   P->count++;

   return r;
}

int delFromPoolInternal (pool *P, regind *prev, regind *target, regind *next)
{
   /* check if head */
   if (prev == NULL)
   {
      P->head = next;
   }
   /* check if tail */
   if (next == NULL)
   {
      P->tail == prev;
   }
   else
   {
      prev->link = next;
   }

   //free (target->str);
   //free (target->byMethod);
   //free (target->root);
   //free (target);

   P->count--;

   return 0;
}

int delFromPool (pool *P, char *strTarget)
{

   regind *prev, *curr, *next;
   prev = NULL;
   curr = NULL;
   next = NULL;

   int max = P->count;
   int i;

   if (max == 0)
   {
      return -1;
   }

   prev = NULL;
   curr = P->head;
   next = curr->link;


   for (i = 0; i < max; i++)
   {
      if (strlen (curr->str) == strlen (strTarget))
      {
         if (strcmp (curr->str, strTarget) == 0)
         {
            /* found it */
            delFromPoolInternal (P, prev, curr, next);
            return 1;
         }
      }
            
      prev = curr;
      curr = prev->link;
      next = curr->link;
   }

   if (strlen (curr->str) == strlen (strTarget))
   {
      if (strcmp (curr->str, strTarget) == 0)
      {
         /* found it */
         delFromPoolInternal (P, prev, curr, next);
         return 1;
      }
   }

   return -1;
}


regind *searchPool (pool *P, char *strTarget)
{
   regind *curr;
   
   int max = P->count;
   int i;

   if (max == 0)
   {
      return NULL;
   }

   curr = P->head;
   if (strlen (curr->str) == strlen (strTarget))
   {
      if (strcmp (curr->str, strTarget) == 0)
      {
         /* found it */
         return curr;
      }
   }

   for (i = 0; i < max; i++)
   {
      curr = curr->link;
      
      if (strlen (curr->str) == strlen (strTarget))
      {
         if (strcmp (curr->str, strTarget) == 0)
         {
            /* found it */
            return curr;
         }
      }
   }

   return NULL;
}

regind *ithPoolMember (pool *P, int i)
{
   if (i > P->count)
   {
      return NULL;
   }

   regind *cur = P->head;
   int j;
   for (j = 0; j < i; j++)
   {
      cur = cur->link;
   }

   return cur;
}
   


int regexlist_driver ()
{
   /* test out all of the function */

   printf ("Initiating test\n");

   /* init pool */
   pool *POOL = initPool ();

   /* search an empty pool */
   searchPool (POOL, "testing");
   
   /* delete from an empty pool */
   delFromPool (POOL, "testing");

   /* add to pool */
   addToPool (POOL, "first", 0);

   /* search for that regind */
   regind * res = searchPool (POOL, "first");
   if (res != NULL)
   {
      printf ("RES: %s\n", res->str);
   }
   
   /* delete that regind */
   int iRes =  delFromPool (POOL, "first");

   /* search for that regind */
   res = searchPool (POOL, "first");
   if (res != NULL)
   {
      printf ("RES: %s\n", res->str);
   }
   else
   {
      printf ("DELETED\n");
   }

   /* add to pool */
   addToPool (POOL, "moomintroll", 0);
   addToPool (POOL, "moominpapa", 0);
   addToPool (POOL, "moominmama", 0);

   /* print pool */
   printPool (POOL);

   /* delete from pool */
   delFromPool (POOL, "moominpapa");

   /* get ith pool member */
   res = ithPoolMember (POOL, 1);
   if (res != NULL)
   {
      printf ("2nd member: %s\n", res->str);
   }
   else
   {
      printf ("No 2nd member found\n");
   }
   /* print pool */
   printPool (POOL);

   /* copy pool */
   //pool *COPY = copyPool (POOL);
   /*  print pool */
   //printf ("COPY\n");
   //printPool (COPY);

   return 0;
}


#endif
   




